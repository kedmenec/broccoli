import React from "react";
import "./styles.css";

export const Footer = () => {
  return (
    <div className="Footer">
      <div>
        Made with <img src="broccoli_small.png" alt="brocolli" /> in Melbourne
      </div>
      <div>&#169; 2017, All Rights Reserved.</div>
    </div>
  );
};
