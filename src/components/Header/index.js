import React from "react";
import "./styles.css";

export const Header = props => {
  return <div className="Header">{props.children}</div>;
};
