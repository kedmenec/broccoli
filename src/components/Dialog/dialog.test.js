import React from "react";
import ReactDOM from "react-dom";
import { Dialog } from "./index.js";
import { mount } from "enzyme";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Dialog />, div);
});

it("renders name error", () => {
  const component = mount(<Dialog show={true} />).setState({ name: "ti" });
  component
    .children()
    .find(".Button")
    .simulate("click");
  expect(component.find("div.ErrorBox").text()).toEqual(
    "Given name must be at least three characters!"
  );
});

it("renders invalid email error", () => {
  const component = mount(<Dialog show={true} />).setState({
    email: "asdfd",
    confirmEmail: "asdfd"
  });
  component
    .children()
    .find(".Button")
    .simulate("click");
  expect(component.find("div.ErrorBox").text()).toEqual(
    "Please provide an email in the correct format"
  );
});

it("renders email mismatch error", () => {
  const component = mount(<Dialog show={true} />).setState({
    email: "test@example.com",
    confirmEmail: "test@example.net"
  });
  component
    .children()
    .find(".Button")
    .simulate("click");
  expect(component.find("div.ErrorBox").text()).toEqual(
    "The provided email addresses dont match!"
  );
});
