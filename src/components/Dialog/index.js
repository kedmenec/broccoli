import React, { Component } from "react";
import { Button } from "../Button";
import "./style.css";

export class Dialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Defaults for testing
      // name: "test",
      // email: "usedemail@airwallex.com",
      // confirmEmail: "usedemail@airwallex.com",
      name: "",
      email: "",
      confirmEmail: "",
      error: null,
      requesting: false
    };
  }

  handleInputChange = event => {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: value
    });
  };

  validateInput = () => {
    const emailsMatch = this.state.email === this.state.confirmEmail;
    const validName = this.state.name.length >= 3;

    if (!validName) {
      this.setState({ error: "Given name must be at least three characters!" });
    } else if (!emailsMatch) {
      this.setState({ error: "The provided email addresses dont match!" });
    } else if (
      !/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(this.state.email)
    ) {
      this.setState({ error: "Please provide an email in the correct format" });
    } else {
      this.setState({
        requesting: true,
        error: null
      });
      const API_URL =
        "https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth";
      const payload = { name: this.state.name, email: this.state.email };

      fetch(API_URL, {
        method: "post",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(payload)
      })
        .then(response => {
          console.log("Response", response);
          return response.json();
        })
        .then(data => {
          console.log("Data", data);
          if (data.errorMessage) {
            this.setState({ error: data.errorMessage, requesting: false });
          } else {
            this.props.handleClick({ success: true, name: this.state.name });
          }
        });
    }
  };

  handleKeyPress = event => {
    if (event.key === "Enter") {
      this.validateInput();
    } else if (event.key === "Escape") {
      this.props.handleClick({ success: false });
    }
  };

  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyPress, false);
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyPress, false);
  }
  render() {
    const boundInput = (name, placeholder) => {
      // Shortcut function for input with actions attached
      return (
        <input
          type="text"
          name={name}
          placeholder={placeholder}
          onChange={this.handleInputChange}
          value={this.state[name]}
          className="Input"
          onKeyPress={this.handleKeyPress}
        />
      );
    };

    return this.props.show ? (
      <div className="Dialog">
        <div className="DialogHeader">Request an invite</div>
        {boundInput("name", "Name")}
        {boundInput("email", "Email")}
        {boundInput("confirmEmail", "Confirm Email")}
        {this.state.error && <div className="ErrorBox">{this.state.error}</div>}
        {this.state.requesting && (
          <div className="MessageBox">Requesting...</div>
        )}

        <div style={{ marginTop: "auto" }}>
          <Button text="Send" handleClick={this.validateInput} />
        </div>
      </div>
    ) : null;
  }
}
