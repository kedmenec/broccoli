import React from "react";
import "./style.css";

export const Button = props => {
  return (
    <div className={"Button"} onClick={props.handleClick}>
      {props.text}
    </div>
  );
};
