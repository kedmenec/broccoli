import React, { Component } from "react";
import { Dialog } from "./components/Dialog";
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { Button } from "./components/Button";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dialogOpen: false,
      error: false,
      successMessage: null
    };
  }

  handleClick = resp => {
    console.log(resp);
    if (resp.success === true) {
      this.setState({
        dialogOpen: !this.state.dialogOpen,
        successMessage: `Thanks ${resp.name}, we'll be in touch!`
      });
    } else {
      this.setState({
        dialogOpen: !this.state.dialogOpen
      });
    }
  };

  render() {
    return (
      <div className={"App"}>
        <Header>Brocolli & Co.</Header>
        <div className={"Main"}>
          <div className={"Heading"}>A better way</div>
          <div className={"Heading"}>to enjoy every day.</div>
          <div className={"SubHeading"}>
            Be the first to know when we launch.
          </div>
          {!this.state.successMessage ? (
            <Button
              text={this.state.dialogOpen ? "Close" : "Request an invite"}
              handleClick={this.handleClick}
            />
          ) : (
            <div className="Success">{this.state.successMessage}</div>
          )}
        </div>
        <Footer />
        <Dialog show={this.state.dialogOpen} handleClick={this.handleClick} />
      </div>
    );
  }
}

export default App;
