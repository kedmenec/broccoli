# Broccoli Co. WebApp Readme

## Install dependencies

`yarn install`
or
`npm install`

## To build a production ready version of the webpage, run

`yarn build`
or
`npm run build`

## To run the webpage locally, run

`yarn start`
or
`npm start`

## To run all tests, run

`yarn test`
or
`npm run test`